import '@testing-library/jest-dom';
import Login from '../../src/components/Login';
import React from 'react';

import { act } from 'react-dom/test-utils';
import { createTheme } from '@mui/material/styles';
import { fireEvent, render, screen } from '@testing-library/react';

describe('Login Component', () => {
  const theme = createTheme();
  let loggedIn = false;

  const login = (credentials: { username: string, password: string }) => {
    loggedIn = true;
  };

  beforeEach(() => {
    loggedIn = false;
  });

  it('renders the login form', () => {
    render(<Login loginFn={login} theme={theme} />);

    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getAllByLabelText(/password/i);
    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(usernameInput).toBeInTheDocument();

    for (const element of passwordInput) {
      expect(element).toBeInTheDocument();
    }

    expect(loginButton).toBeInTheDocument();
  });

  it('password visibility can be toggled', () => {
    render(<Login loginFn={login} theme={theme} />);

    const passwordInput = screen.getAllByLabelText(/password/i);

    for (const element of passwordInput) {
      expect(['password', 'button']).toContain(element.getAttribute('type'));
    }

    const toggleVisibility = passwordInput.find(element => element.getAttribute('type') == 'button');

    act(() => {
      if (toggleVisibility) {
        toggleVisibility.click();
      }
    });

    for (const element of passwordInput) {
      expect(['text', 'button']).toContain(element.getAttribute('type'));
    }

    act(() => {
      if (toggleVisibility) {
        toggleVisibility.click();
      }
    });

    for (const element of passwordInput) {
      expect(['password', 'button']).toContain(element.getAttribute('type'));
    }
  });

  it('validates that a username and password were provided', () => {
    render(<Login loginFn={login} theme={theme} />);

    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(loggedIn).toBe(false);

    act(() => {
      fireEvent.click(loginButton);
    });

    expect(loggedIn).toBe(false);
  });

  it('login function is called', () => {
    render(<Login loginFn={login} theme={theme} />);

    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getAllByLabelText(/password/i);
    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(loggedIn).toBe(false);

    act(() => {
      fireEvent.change(usernameInput, {target: {value: 'username'}});
      fireEvent.change(passwordInput[0], {target: {value: 'password'}});
      fireEvent.click(loginButton);
    }).then(() => expect(loggedIn).toBe(true));
  });
});
