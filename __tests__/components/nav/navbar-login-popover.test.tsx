import '@testing-library/jest-dom';
import React from 'react';

import { BasicUser } from '../../../src/utils';
import { NavbarLoginPopover } from '../../../src/components/nav';
import { act } from 'react-dom/test-utils';
import { createTheme } from '@mui/material/styles';
import { render, screen } from '@testing-library/react';

describe('Navbar Login Popover', () => {
  const theme = createTheme();
  let loggedIn = false;

  const login = (credentials: { username: string, password: string }) => {
    loggedIn = true;
  };

  it('renders a login popover on a navbar when the user is unauthenticated', () => {
    render(<NavbarLoginPopover loginFn={login} theme={theme} />);

    const btn = screen.getByRole('button');

    expect(btn).toBeInTheDocument();

    act(() => {
      btn.click();
    });

    const usernameInput = screen.getByLabelText(/username/i);
    const passwordInput = screen.getAllByLabelText(/password/i);
    const loginButton = screen.getByRole('button', { name: 'Login' });

    expect(usernameInput).toBeInTheDocument();

    for (const element of passwordInput) {
      expect(element).toBeInTheDocument();
    }

    expect(loginButton).toBeInTheDocument();
  });

  it('renders a login popover on a navbar when the user is authenticated', () => {
    const user: BasicUser = {
      id: 'test-id',
      firstName: 'Test',
      lastName: 'User'
    }

    render(<NavbarLoginPopover loginFn={login} user={user} theme={theme} />);

    const avatar = screen.getByText('TU');

    expect(avatar).toBeInTheDocument();
  });
});
