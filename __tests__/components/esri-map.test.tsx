import '@testing-library/jest-dom';
import React from 'react';

import { EsriMap } from '../../src/components';
import { render, screen } from '@testing-library/react';

let finalAcode = '';

function initializeMap(acode: string, setLoading: (loading: boolean) => void) {
  finalAcode = acode;
}

describe('ESRI map', () => {
  it('renders a map with a loading bar', () => {
    const queryValue = 'B-GRAM';

    expect(finalAcode).toEqual('');

    render(<EsriMap queryValue={queryValue} initializeMap={initializeMap} height={400} />);
    const progressBar = screen.getByRole('progressbar');

    expect(finalAcode).toEqual(queryValue);
    expect(progressBar).toBeInTheDocument();
  });
});
