import { Route } from './types';

/**
 * Checks if an object is a valid {@link Route} object.
 *
 * @param {any} x The object to check
 * @returns {boolean} Whether or not the object is a valid {@link Route} object
 */
export function isRoute(x: any): x is Route {
  const routeToTest = x as Route;
  return routeToTest.name !== undefined && routeToTest.paths !== undefined;
}

/**
 * Creates an avatar with a user's first and last initial
 *
 * @param {string} name The user's name
 * @param {string} bgColor The background color of the avatar
 * @param {string} color The color of the text in the avatar
 */
export function stringAvatar(name: string, bgColor = '#fff', color = '#000') {
  return {
    sx: {
      bgcolor: bgColor,
      color: color,
    },
    children: name.includes(' ')
      ? `${name.split(' ')[0][0].toUpperCase()}${name
          .split(' ')[1][0]
          .toUpperCase()}`
      : name[0],
  };
}
