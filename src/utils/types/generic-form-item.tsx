import { AdornmentProps } from '../../components';

export type ClickableErrorMesssage = {
  text: string;
  action: () => void;
};

export type GenericFormItem = {
  fcName: string;
  name: string;
  type: 'custom' | 'date' | 'dropdown' | 'multiline' | 'number' | 'text';
  value?: any;
  tooltip?: string;
  tooltipZIndex?: number;
  // only used if type === 'dropdown'
  options?: Array<any>;
  // only used if type === 'dropdown'
  dropdownLabelFunctionPred?: (option: any) => string;
  // only used if type === 'dropdown'
  optionKey?: string;
  // only used if type === 'dropdown'
  optionValueKey?: string;
  // only used if type === 'dropdown'
  optionValueKeyPrefix?: string;
  // only used if type === 'dropdown'
  multiple?: boolean;
  required?: boolean;
  disabled?: boolean;
  inputProps?: Omit<AdornmentProps, 'adornment'>;
  error?: boolean;
  errorMessage?: string;
  clickableErrorMessage?: ClickableErrorMesssage;
  // only used if type === 'custom'
  component?: JSX.Element;
  // whether or not this field is not part of a form. defaults to false
  standalone?: boolean;
  // only used if type === 'dropdown' and asynchronous searching should be enabled
  onInputChange?: (event: React.ChangeEvent<{}>, newValue: string) => void;
  // The name of the group the field should be a part of (for separating forms)
  formGroup?: string;
  // Whether or not the field should be in a collapsible group. Only able to be used in conjunction when `formGroup` is set. Collapsible fields will always be placed under non-collapsible fields
  collapsible?: boolean;
};
