import { ReactElement } from 'react';

export type CustomTab = {
  /** The name of the tab */
  title: string;
  /** An MUI icon to display on the tab */
  icon: ReactElement;
  /** A React component containing the content that should be displayed for each tab */
  content: ReactElement;
};
