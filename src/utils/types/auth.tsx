export type LoginFn = (
  credentials: { username: string; password: string },
  callbackUrl: string,
) => void;

export type LogoutFn = (callbackUrl?: string) => void;
