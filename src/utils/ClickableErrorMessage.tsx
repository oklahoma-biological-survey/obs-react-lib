import Link from '@mui/material/Link';
import type { ClickableErrorMesssage } from './types';

type ClickableErrorMesssageComponentProps = ClickableErrorMesssage & {
  errorMessage: string;
};

export default function ClickableErrorMesssageComponent(
  props: ClickableErrorMesssageComponentProps,
) {
  return (
    <p
      style={{
        color: '#d32f2f',
        fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
        fontWeight: 400,
        fontSize: '0.75rem',
        lineHeight: 1.66,
        letterSpacing: '0.03333em',
        textAlign: 'left',
        marginTop: '3px',
        marginRight: 0,
        marginBottom: 0,
        marginLeft: 0,
      }}
    >
      {props.errorMessage}.{' '}
      <Link component="button" type="button" onClick={props.action}>
        {props.text}
      </Link>
    </p>
  );
}
