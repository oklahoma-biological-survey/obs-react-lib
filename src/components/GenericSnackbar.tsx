'use client';

import CloseIcon from '@mui/icons-material/Close';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Slide, { SlideProps } from '@mui/material/Slide';
import Snackbar from '@mui/material/Snackbar';
import React from 'react';
import { forwardRef, useImperativeHandle, useState } from 'react';

export type GenericSnackbarAction = {
  name: string;
  fn: () => void;
};

export type GenericSnackbarProps = {
  /** Reference to React object */
  ref: React.MutableRefObject<any>;
  /** The message to display on the snackbar */
  message: string;
  /**
   * The severity of the alert shown in the snackbar
   *
   * @default 'info'
   */
  severity?: 'error' | 'info' | 'success';
  /**
   * The duration the snackbar should be displayed
   *
   * @defaultValue 3000
   */
  duration?: number;
  /** The name of the button and the function it calls for an action displayed in the snackbar */
  action?: GenericSnackbarAction;
};

const DEFAULT_PROPS: GenericSnackbarProps = {
  ref: null,
  message: '',
  severity: 'info',
  duration: 3000,
};

export type GenericSnackbarRef = {
  open: () => void;
};

/* istanbul ignore next */
function SlideTransition(props: SlideProps) {
  return <Slide {...props} direction="up" />;
}

/**
 * Displays an MUI Snackbar on the user's screen.
 * Based on [MUI Snackbar](https://mui.com/material-ui/react-snackbar/)
 *
 * @param {GenericSnackbarProps} props
 *
 * @example
 * import Button from '@mui/material/Button';
 *
 * import { GenericSnackbar } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { useRef, useState } from 'react';
 *
 * export function Layout() {
 *   const [snackbarMessage, setSnackbarMessage] = useState('');
 *   const snackbarRef = useRef(null);
 *   const message = 'message';
 *
 *   const fn = () => {
 *     setSnackbarMessage(message);
 *   };
 *
 *   return (
 *     <Button onClick={fn}>Click Me!</Button>
 *     <GenericSnackbar ref={snackbarRef} message={snackbarMessage} />
 *   );
 * }
 */
const GenericSnackbar = forwardRef<GenericSnackbarRef, GenericSnackbarProps>(
  (props: GenericSnackbarProps, ref) => {
    const [snackbarOpen, setSnackbarOpen] = useState(false);

    useImperativeHandle(ref, () => ({
      open() {
        setSnackbarOpen(true);
      },
    }));

    props = {
      ...props,
      severity: !props.severity ? DEFAULT_PROPS.severity : props.severity,
      duration: !props.duration ? DEFAULT_PROPS.duration : props.duration,
    };

    const handleClose = () => {
      setSnackbarOpen(false);
    };

    const action = (
      <React.Fragment>
        <Button color="primary" size="small" onClick={props.action?.fn}>
          {props.action?.name}
        </Button>
        <IconButton
          size="small"
          aria-label="close"
          color="inherit"
          onClick={handleClose}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      </React.Fragment>
    );

    return (
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        autoHideDuration={props.duration}
        TransitionComponent={SlideTransition}
        open={snackbarOpen}
        onClose={handleClose}
      >
        <Alert
          onClose={handleClose}
          severity={props.severity}
          elevation={6}
          sx={{ width: '100%' }}
          action={action}
        >
          {props.message}
        </Alert>
      </Snackbar>
    );
  },
);

export default GenericSnackbar;
