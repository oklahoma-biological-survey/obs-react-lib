'use client';

import Container from '@mui/material/Container';
import LoginForm from './LoginForm';

import { Theme } from '@mui/material';
import { usePathname } from 'next/navigation';
import type { LoginFn } from '../utils/types/auth';

type LoginProps = {
  loginFn: LoginFn;
  theme: Theme;
};

export default function Login(props: LoginProps) {
  const pathname = usePathname();

  return (
    <Container sx={{ mb: 2 }}>
      <LoginForm
        loginFn={props.loginFn}
        callbackUrl={pathname as string}
        theme={props.theme}
        invalidCredentials={false}
        width="100%"
      />
    </Container>
  );
}
