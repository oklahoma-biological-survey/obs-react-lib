'use client';

import LoginIcon from '@mui/icons-material/Login';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import IconButton from '@mui/material/IconButton';
import GenericForm from './GenericForm';

import { Theme } from '@mui/material';
import { useState } from 'react';
import type { GenericFormItem } from '../utils/types';
import type { LoginFn } from '../utils/types/auth';
import { buildInputAdornmentProps } from './fields';

import * as yup from 'yup';

export type LoginFormProps = {
  /** Callback function to be called when the login form is submitted */
  loginFn: LoginFn;
  /** The callback URL to redirect to after logging in */
  callbackUrl: string;
  /** Whether or not the user provided invalid credentials */
  invalidCredentials: boolean;
  /** An optional MUI theme */
  theme: Theme;
  /**
   * The width of the form
   *
   * @defaultValue 75%
   */
  width?: string;
};

/**
 * Generates a login form
 *
 * @param {LoginFormProps} props
 * @example
 * import { LoginForm } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { theme } from 'utils/theme';
 * import { useRouter } from 'next/router';
 * import { useSession } from 'next-auth/react';
 *
 * export default function Login() {
 *   const { status } = useSession();
 *   const router = useRouter();
 *   let invalidCredentials = false;
 *
 *   const login = (credentials: { username: string, password: string }) => console.log(credentials);
 *
 *   if (status === 'authenticated') {
 *     router.push('/');
 *   }
 *
 *   if (router.query.error && router.query.error === 'CredentialsSignin') {
 *     invalidCredentials = true;
 *   }
 *
 *   return(
 *     <LoginForm loginFn={login} theme={theme} invalidCredentials={invalidCredentials} />
 *   );
 * }
 */
export default function LoginForm(props: LoginFormProps) {
  const [showPassword, setShowPassword] = useState(false);

  props = {
    ...props,
    width: !props.width ? '75%' : props.width,
  };

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  const passwordInputProps = buildInputAdornmentProps({
    type: showPassword ? 'text' : 'password',
    position: 'end',
    adornment: (
      <IconButton
        aria-label="toggle-password-visibility"
        onClick={handleClickShowPassword}
        onMouseDown={handleMouseDownPassword}
      >
        {showPassword ? <VisibilityOff /> : <Visibility />}
      </IconButton>
    ),
  });

  const fields: Array<GenericFormItem> = [
    { fcName: 'username', name: 'Username', type: 'text', value: '' },
    {
      fcName: 'password',
      name: 'Password',
      type: 'text',
      value: '',
      inputProps: passwordInputProps,
    },
  ];

  const onSubmit = async (values: any) => {
    props.loginFn(values, props.callbackUrl);
  };

  const validationSchema = yup.object({
    username: yup.string().required('Username is required'),
    password: yup.string().required('Password is required'),
  });

  return (
    <>
      <GenericForm
        title="Login"
        fields={fields}
        onSubmit={onSubmit}
        submitBtnConfig={{
          title: 'Login',
          icon: <LoginIcon />,
        }}
        validationSchema={validationSchema}
        width={props.width}
        borderColor={props.theme.palette.primary.main}
        errorMsg={props.invalidCredentials ? 'Invalid credentials' : null}
      />
    </>
  );
}
