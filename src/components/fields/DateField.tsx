'use client';

import FormControl from '@mui/material/FormControl';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import dayjs, { Dayjs } from 'dayjs';

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { FormikProps } from 'formik';
import { useState } from 'react';
import { GenericFormItem } from '../../utils';

export type DateFieldProps = {
  /** The name of the form control to assign to this field */
  fcName: string;
  /**
   * The type of field
   *
   * @defaultValue 'date'
   */
  type: GenericFormItem['type'];
  /**
   * The name of the field, displayed on the field's label
   *
   * @defaultValue ''
   */
  name?: string;
  /**
   * The variant this field should be displayed as
   *
   * @defaultValue standard
   */
  variant?: 'standard' | 'outlined';
  /**
   * Whether or not this field is required in its form
   *
   * @defaultValue false
   */
  required?: boolean;
  /**
   * The width of the field
   *
   * @defaultValue 100%
   */
  width?: string;
  /**
   * The value of the field
   *
   * @defaultValue ''
   */
  value?: string;
  /**
   * Whether or not this field is disabled
   *
   * @defaultValue false
   */
  disabled?: boolean;
  /**
   * Whether or not this field has an error
   *
   * @defaultValue false
   */
  error?: boolean;
  /**
   * The text of any errors
   *
   * @default null
   */
  errorMessage?: string | null;
  /** The formik form props */
  formik?: FormikProps<any>;
  /**
   * Optional text to be displayed in a tooltip above the field
   *
   * @default '''
   */
  tooltip?: string;
  /**
   * The z-index of the tooltip
   */
  tooltipZIndex?: number;
  /**
   * Callback function to perform an action when the value changes
   *
   * @defaultValue null
   */
  onValueChange?: ((event: Dayjs) => void) | null;
};

const DEFAULT_PROPS: DateFieldProps = {
  fcName: '',
  type: 'date',
  name: '',
  variant: 'standard',
  required: false,
  width: '100%',
  value: '',
  disabled: false,
  error: false,
  errorMessage: null,
  formik: undefined,
  tooltip: '',
};

export default function DateField(props: DateFieldProps) {
  props = {
    ...props,
    variant: !props.variant ? DEFAULT_PROPS.variant : props.variant,
    width: !props.width ? DEFAULT_PROPS.width : props.width,
    disabled: !props.disabled ? DEFAULT_PROPS.disabled : props.disabled,
    error: !props.error ? DEFAULT_PROPS.error : props.error,
    errorMessage: !props.errorMessage
      ? DEFAULT_PROPS.errorMessage
      : props.errorMessage,
    tooltip: !props.tooltip ? DEFAULT_PROPS.tooltip : props.tooltip,
  };

  const [value, setValue] = useState<Dayjs | string | null>(
    props.value ? dayjs(props.value) : null,
  );

  const handleChange = (changedValue: Dayjs) => {
    if (!changedValue) {
      props.formik?.setFieldValue(props.fcName, '');
      setValue('');
      return;
    }

    if (changedValue.isValid()) {
      props.formik?.setFieldValue(props.fcName, changedValue.format());
      setValue(changedValue);
      props.onValueChange(changedValue);
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Tooltip
        title={props.tooltip}
        placement="right"
        TransitionComponent={Zoom}
        enterDelay={500}
        enterNextDelay={100}
        PopperProps={{
          style: {
            zIndex: props.tooltipZIndex,
          },
        }}
        arrow
      >
        <FormControl
          variant={props.variant}
          sx={{ mt: 1.5, width: props.width }}
        >
          <DatePicker
            label={props.name}
            disabled={props.disabled}
            value={value}
            onChange={handleChange}
            slotProps={{
              textField: {
                error: props.error,
                helperText: props.errorMessage,
                variant: props.variant,
                name: props.fcName,
              },
            }}
            disableFuture
          />
        </FormControl>
      </Tooltip>
    </LocalizationProvider>
  );
}
