'use client';

import Box from '@mui/material/Box';
import FormControl from '@mui/material/FormControl';
import Grid from '@mui/material/Grid';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';
import dayjs, { Dayjs } from 'dayjs';

import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { useState } from 'react';

export type DateRangeFieldProps = {
  /**
   * The variant this field should be displayed as
   *
   * @defaultValue standard
   */
  variant?: 'standard' | 'outlined';
  /**
   * The value of the start date
   *
   * @defaultValue ''
   */
  startDateValue?: string;
  /**
   * Callback function to perform an action when the value of the start date changes
   *
   * @defaultValue null
   */
  onStartDateValueChange: (value: string) => void;
  /**
   * The value of the end date
   *
   * @defaultValue ''
   */
  endDateValue?: string;
  /**
   * Callback function to perform an action when the value of the end date changes
   *
   * @defaultValue null
   */
  onEndDateValueChange: (value: string) => void;
  /**
   * Optional text to be displayed in a tooltip above the start date
   *
   * @default '''
   */
  startDateTooltip?: string;
  /**
   * Optional text to be displayed in a tooltip above the end date
   *
   * @default '''
   */
  endDateTooltip?: string;
};

const DEFAULT_PROPS: Omit<
  DateRangeFieldProps,
  'onStartDateValueChange' | 'onEndDateValueChange'
> = {
  variant: 'standard',
  startDateValue: '',
  endDateValue: '',
  startDateTooltip: '',
  endDateTooltip: '',
};

/**
 * This is separate from the `DateField` component, even though a lot of code is duplicated, as the MUI
 * date range field requires an MUI X Pro license. As such, want to keep this completely separate from
 * the component that wraps the MUI DatePicker that does not require a license.
 *
 * Furthermore, this component is only meant to be used in a standalone fashion (i.e. outside of a form).
 */
export default function DateRangeField(props: DateRangeFieldProps) {
  props = {
    ...props,
    variant: !props.variant ? DEFAULT_PROPS.variant : props.variant,
    startDateTooltip: !props.startDateTooltip
      ? DEFAULT_PROPS.startDateTooltip
      : props.startDateTooltip,
    endDateTooltip: !props.endDateTooltip
      ? DEFAULT_PROPS.endDateTooltip
      : props.endDateTooltip,
  };

  const [startDateValue, setStartDateValue] = useState<Dayjs | string | null>(
    props.startDateValue ? dayjs(props.startDateValue) : null,
  );

  const [endDateValue, setEndDateValue] = useState<Dayjs | string | null>(
    props.endDateValue ? dayjs(props.endDateValue) : null,
  );

  const handleStartDateChange = (changedValue: Dayjs) => {
    if (!changedValue) {
      setStartDateValue(null);
      props.onStartDateValueChange('');
      return;
    }

    if (changedValue.isValid() && props.onStartDateValueChange) {
      setStartDateValue(changedValue);
      props.onStartDateValueChange(changedValue.format());
    }
  };

  const handleEndDateChange = (changedValue: Dayjs) => {
    if (!changedValue) {
      setEndDateValue(null);
      props.onStartDateValueChange('');
      return;
    }

    if (changedValue.isValid() && props.onEndDateValueChange) {
      setEndDateValue(changedValue);
      props.onEndDateValueChange(changedValue.format());
    }
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <Tooltip
              title={props.startDateTooltip}
              placement="right"
              TransitionComponent={Zoom}
              enterDelay={500}
              enterNextDelay={100}
              arrow
            >
              <FormControl
                variant={props.variant}
                sx={{ mt: 1.5, width: '100%' }}
              >
                <DatePicker
                  label="Start Date"
                  value={startDateValue}
                  onChange={handleStartDateChange}
                  slotProps={{
                    textField: {
                      variant: props.variant,
                      name: 'startDate',
                    },
                  }}
                  disableFuture
                />
              </FormControl>
            </Tooltip>
          </Grid>
          <Grid item xs={6}>
            <Tooltip
              title={props.endDateTooltip}
              placement="right"
              TransitionComponent={Zoom}
              enterDelay={500}
              enterNextDelay={100}
              arrow
            >
              <FormControl
                variant={props.variant}
                sx={{ mt: 1.5, width: '100%' }}
              >
                <DatePicker
                  label="End Date"
                  value={endDateValue}
                  onChange={handleEndDateChange}
                  slotProps={{
                    textField: {
                      variant: props.variant,
                      name: 'endDate',
                    },
                  }}
                  disableFuture
                />
              </FormControl>
            </Tooltip>
          </Grid>
        </Grid>
      </Box>
    </LocalizationProvider>
  );
}
