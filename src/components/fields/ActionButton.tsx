'use client';

import LoadingButton from '@mui/lab/LoadingButton';

import { ReactElement } from 'react';

export type ActionButtonProps = {
  /** The title to display on the button */
  title: string;
  /** Callback function that is called when the button is clicked */
  onButtonClick?: () => void;
  /** Icon to display at the beginning of the button */
  startIcon?: ReactElement;
  /**
   * Whether or not this button should be disabled
   *
   * @defaultValue false
   */
  disabled?: boolean;
  /**
   * Whether or not this button is performing a submit action
   *
   * @defaultValue false
   */
  submitting?: boolean;
};

/**
 * Component that generates a button.
 * Based on [MUI Button.](https://mui.com/material-ui/react-button/)
 *
 * @param {ActionButtonProps} props
 *
 * @example
 * import { ActionButton } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 *
 * export function Main() {
 *   const fn = () => {
 *     // do something
 *   };
 *
 *   return (
 *     <ActionButton title='Click Me!' onButtonClick={fn} />
 *   );
 * }
 */
export default function ActionButton(props: ActionButtonProps) {
  props = {
    ...props,
    disabled: !props.disabled ? false : props.disabled,
    submitting: !props.submitting ? false : props.submitting,
  };

  const buttonId = `${props.title}-action`;

  const handleClick = () => {
    if (props.onButtonClick) {
      props.onButtonClick();
    }
  };

  return (
    <LoadingButton
      id={buttonId}
      type="submit"
      onClick={handleClick}
      sx={{ mt: 3, mr: 1 }}
      startIcon={props.startIcon}
      loading={props.submitting}
      loadingPosition="start"
      variant="contained"
      disabled={props.disabled}
    >
      {props.title}
    </LoadingButton>
  );
}
