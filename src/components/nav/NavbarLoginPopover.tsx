'use client';

import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import Popover from '@mui/material/Popover';
import Login from '../Login';

import { Theme } from '@mui/material';
import { useState } from 'react';
import { BasicUser, stringAvatar } from '../../utils';
import type { LoginFn, LogoutFn } from '../../utils/types/auth';
import { NavbarDropdownOptions } from './NavbarDropdown';

export type NavbarLoginPopoverProps = {
  loginFn: LoginFn;
  logoutFn: LogoutFn;
  user?: BasicUser;
  theme: Theme;
};

/**
 * Displays a login form in a popover from a navbar.
 *
 * @remarks
 * For internal use only
 */
export default function NavbarLoginPopover(props: NavbarLoginPopoverProps) {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);

  const openLoginPopover = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const closeLoginPopover = () => {
    setAnchorEl(null);
  };

  const loginPopoverOpen = Boolean(anchorEl);
  const loginPopoverId = loginPopoverOpen ? 'login-popover' : undefined;

  if (!props.user) {
    return (
      <>
        <IconButton
          aria-describedby={loginPopoverId}
          sx={{ color: 'white' }}
          aria-label="login"
          onClick={openLoginPopover}
        >
          <AccountCircleIcon sx={{ height: 40, width: 40 }} />
        </IconButton>
        <Popover
          key={loginPopoverId}
          id={loginPopoverId}
          open={loginPopoverOpen}
          anchorEl={anchorEl}
          onClose={closeLoginPopover}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <Login loginFn={props.loginFn} theme={props.theme} />
        </Popover>
      </>
    );
  } else {
    const userName: string =
      props.user.firstName && props.user.lastName
        ? [props.user.firstName, props.user.lastName].join(' ')
        : 'Application User';

    return (
      <>
        <IconButton
          aria-describedby={loginPopoverId}
          sx={{ color: 'white' }}
          aria-label="login"
          onClick={openLoginPopover}
        >
          <Avatar
            {...stringAvatar(
              userName,
              '#fff',
              props.theme.palette.primary.main,
            )}
          />
        </IconButton>
        <Popover
          key={loginPopoverId}
          id={loginPopoverId}
          open={loginPopoverOpen}
          anchorEl={anchorEl}
          onClose={closeLoginPopover}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
        >
          <NavbarDropdownOptions
            options={[
              {
                name: 'View Profile',
                paths: [`/user/${props.user.id}`],
              },
              {
                name: 'Logout',
                paths: [],
                fn: props.logoutFn,
              },
            ]}
            theme={props.theme}
          />
        </Popover>
      </>
    );
  }
}
