'use client';

import MenuIcon from '@mui/icons-material/Menu';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Image from 'next/image';
import Link from 'next/link';

import { Theme } from '@mui/material';
import { usePathname } from 'next/navigation';
import { useState } from 'react';
import { NavbarLoginPopover } from '.';
import { Route } from '../../utils/types';
import { NavbarButton, NavbarExpandButton } from './NavbarButton';
import { NavbarDropdown } from './NavbarDropdown';
import { NavbarExpandOptions } from './NavbarExpandOptions';

export type NavbarProps = {
  /** The name of the site */
  name: string;
  /** The short name of the site (displayed only on mobile) */
  shortName: string;
  /** An array defining the routes for this site */
  routes: Array<Route>;
  /** Optional configuration for the logo (to replace the `name`) */
  logo?: {
    /** Path to the logo file */
    src: string;
    /**
     * An optional height for the logo
     *
     * @defaultValue 50
     */
    height?: number;
    /**
     * An optional width for the logo
     *
     * @defaultValue 50
     */
    width?: number;
  };
  /** The MUI theme to use */
  theme: Theme;
};

/**
 * Component that generates a navbar.
 * Based on [MUI App Bar.](https://mui.com/material-ui/react-app-bar/)
 * @param {NavbarProps} props
 * @example
 * import logo from '<path_to_logo>';
 *
 * import { Navbar } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { Route } from '@oklahoma-biological-survey/obs-react-lib/nextjs/utils/types';
 *
 * const routes: Array<Route> = [
 *   {
 *     name: 'Route One',
 *     paths: ['route-1']
 *   },
 *   {
 *     name: 'Dynamic Route',
 *     paths: ['route-2', 'route-3'],
 *     activePathIndex: 0
 *   },
 *   {
 *     name: 'Menu',
 *     paths: [],
 *     options: [
 *       {
 *         name: 'Option One',
 *         paths: ['option-1'],
 *         divider: true
 *       },
 *       {
 *         name: 'Option Two',
 *         paths: ['option-2']
 *       }
 *     ]
 *   },
 *   {
 *     name: 'Login',
 *     paths: [],
 *     login: true
 *   }
 * ];
 *
 * export function Layout() {
 *   const title = 'Test Nav';
 *
 *   return (
 *     <Navbar
 *       name={title}
 *       shortName='TN'
 *       routes={routes}
 *       logoSrc={{
 *         src: logo.src
 *       }}
 *     />
 *   );
 * }
 */
export default function Navbar(props: NavbarProps) {
  props = { ...props };

  if (props.logo) {
    props.logo = {
      ...props.logo,
      height: !props.logo.height ? 50 : props.logo.height,
      width: !props.logo.width ? 50 : props.logo.width,
    };
  }

  const [drawerOpen, setDrawerOpen] = useState(false);
  const pathname = usePathname();

  const routes = props.routes;

  const closeDrawer = () => {
    setDrawerOpen(false);
  };

  const handleDrawerToggle = () => {
    setDrawerOpen(!drawerOpen);
  };

  const getPath = (route: Route, currentRoute: string) => {
    const paths = route.paths;
    const currentRouteFragments = currentRoute.split('/');
    const prefix = currentRouteFragments[1];
    const location = currentRouteFragments[3];
    const matchingRouteNames = new Array<string>();

    switch (paths.length) {
      case 0:
        return '#';
      case 1:
        return paths[0];
    }

    // Find other routes that have the same prefix
    props.routes.forEach((routeDef) => {
      routeDef.paths.forEach((path) => {
        if (
          path.startsWith(`/${prefix}`) &&
          routeDef.name !== route.name &&
          !matchingRouteNames.includes(routeDef.name)
        ) {
          matchingRouteNames.push(routeDef.name);
        }
      });
    });

    // For this to work properly, the static path MUST be placed at the first position of the paths array
    // The paths also need to be in the order of user navigation
    for (const path of paths) {
      const index = paths.indexOf(path);
      let nextPath = index !== paths.length - 1 ? paths[index + 1] : null;
      const matchingRoutes = props.routes.filter((route) =>
        matchingRouteNames.includes(route.name),
      );

      if (currentRoute === path) {
        route.activePathIndex = index;

        matchingRoutes.map((route) => {
          route.activePathIndex = index;
        });

        return path;
      }

      // Strip off the '/*' from the end of the dynamic path
      if (
        path.endsWith('/*') &&
        currentRoute.startsWith(path.substring(0, path.length - 2))
      ) {
        paths[index] = currentRoute;
        route.activePathIndex = index;

        matchingRoutes.map((route) => {
          route.paths[index] = currentRoute.replace(
            location,
            route.paths[index].split('/')[3],
          );
          route.activePathIndex = index;
        });

        return currentRoute;
      }

      // When the user navigates back, need to ensure the dynamic path ahead of it is replaced with the asterisk
      if (
        nextPath &&
        !nextPath.endsWith('/*') &&
        currentRoute.startsWith(
          nextPath.substring(0, nextPath.lastIndexOf('/')),
        )
      ) {
        nextPath = `${nextPath.substring(0, nextPath.lastIndexOf('/'))}/*`;
        route.paths[index + 1] = nextPath;

        matchingRoutes.map((route) => {
          let nextPath =
            index !== route.paths.length - 1 ? route.paths[index + 1] : null;

          if (
            nextPath &&
            !nextPath.endsWith('/*') &&
            !currentRoute.startsWith(
              nextPath.substring(0, nextPath.lastIndexOf('/')),
            )
          ) {
            nextPath = `${nextPath.substring(0, nextPath.lastIndexOf('/'))}/*`;
            route.paths[index + 1] = nextPath;
          }
        });
      }
    }

    // If we haven't found any path match yet, just return the path at the index of 'activePathIndex'
    if (route.activePathIndex !== undefined) {
      const thisIndex = props.routes.indexOf(route);

      // If we are processing a route that has a lower index than the route that is being navigated on, we need to go process
      // that route first. This is because, the routes are linked together, but the Navbar will process the routes in the order
      // in which they appear in the list. So, if a route is being navigated on that is linked to another route earlier in the
      // list, the earlier route will have an invalid path returned to the Navbar
      matchingRouteNames.forEach((matchingRouteName) => {
        const matchingRouteIndex = props.routes.findIndex(
          (route) => route.name === matchingRouteName,
        );

        if (thisIndex < matchingRouteIndex) {
          getPath(props.routes[matchingRouteIndex], currentRoute);
        }
      });

      return paths[route.activePathIndex];
    }

    throw new Error(
      `Route with name '${route.name}' is configured incorrectly. Please check the 'paths' array to ensure there will be a match.`,
    );
  };

  const title = props.logo?.src ? (
    <Image
      src={props.logo.src}
      height={props.logo.height}
      width={props.logo.width}
      alt={`${props.shortName} Logo`}
    />
  ) : (
    props.name
  );

  const drawer = (
    <Box sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        {props.shortName}
      </Typography>
      <Divider />
      <List>
        {routes.map((route, index) =>
          route.options ? (
            <NavbarExpandOptions
              routes={routes}
              index={index}
              key={index}
              theme={props.theme}
            />
          ) : (
            <ListItem key={index} disablePadding>
              <NavbarExpandButton
                route={route}
                currentRoute={pathname}
                getPath={getPath}
                closeDrawer={closeDrawer}
              />
            </ListItem>
          ),
        )}
      </List>
    </Box>
  );

  const drawerWidth = 240;

  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar component="nav">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{
              flexGrow: 1,
              display: { xs: 'none', sm: 'block' },
              pt: props.logo ? 1 : 0,
            }}
          >
            <Link href="/">{title}</Link>
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {routes.map((route, index) =>
              route.options ? (
                <NavbarDropdown
                  title={route.name}
                  options={route.options}
                  key={index}
                  theme={props.theme}
                />
              ) : route.login && route.loginFn ? (
                <NavbarLoginPopover
                  loginFn={route.loginFn}
                  logoutFn={route.logoutFn}
                  user={route.user}
                  theme={props.theme}
                  key={index}
                />
              ) : (
                <NavbarButton
                  key={index}
                  route={route}
                  currentRoute={pathname}
                  getPath={getPath}
                />
              ),
            )}
          </Box>
        </Toolbar>
      </AppBar>
      <Box component="nav">
        <Drawer
          variant="temporary"
          open={drawerOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
      <Toolbar />
    </Box>
  );
}
