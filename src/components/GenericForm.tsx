'use client';

import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import { Typography } from '@mui/material';
import { FormikErrors, FormikProps, FormikProvider, useFormik } from 'formik';
import { ChangeEvent, ReactElement, RefObject, useEffect } from 'react';
import { ActionButton } from './fields';
import { DateField, GenericDropdown, GenericTextField } from './fields';

import type { GenericFormItem } from '../utils';

export type GenericFormItemProps = GenericFormItem & {
  /** Callback function to handle changes to the field at the form level */
  handleChange?: (e: ChangeEvent<any>) => void;
  /** Same as `handleChange`, but for a `GenericDropdown` (MUI Autocomplete) */
  setValue?: (
    field: string,
    value: any,
    shouldValidate?: boolean,
  ) => Promise<void> | Promise<FormikErrors<any>>;
  /** Whether or not this field should be disabled */
  formDisabled?: boolean;
  /** The formik form props */
  formik?: FormikProps<any>;
};

export type GenericFormProps = {
  /** The title of the form, to be displayed above the form */
  title: string;
  /** An array denoting what types of fields and their names should be included in this form */
  fields: Array<GenericFormItem>;
  /** Callback function that performs an action when the form is submitted */
  onSubmit: (values: any) => Promise<void>;
  /** Callback function to handle validation errors */
  handleValidationError?: () => void;
  /** Whether or not this form should be disabled */
  disabled?: boolean;
  /**
   * Submit button configuration. Consists of a title as a `string`, and an icon as a `ReactElement`
   *
   * @remarks
   * If this value is left undefined, there will not be a submit button displayed at the bottom of the form
   */
  submitBtnConfig?: {
    title: string;
    icon: ReactElement;
  };
  /**
   * Configuration for an action button to be placed in the top right of the form. Same schema as `submitBtnConfig`
   *
   * @remarks
   * If this value is left undefined, there will not be a submit button displayed at the bottom of the form
   */
  actionButtonConfig?: {
    title: string;
    icon: ReactElement;
    action: () => Promise<void>;
  };
  /**
   * Ref used for submitting this form from a parent element.
   *
   * @remarks
   * Cannot use this property in tandem with `submitBtnConfig`
   */
  submitRef?: RefObject<HTMLButtonElement>;
  /**
   * Optionally, a schema used to validate all fields in the form. See [yup](https://github.com/jquense/yup)
   *
   * @defaultValue null
   */
  validationSchema?: any;
  /**
   * The width of the form
   *
   * @defaultValue 75%
   */
  width?: string;
  /**
   * The color of the border of the form
   *
   * @defaultValue #000
   */
  borderColor?: string;
  /**
   * An error message to display under the form
   *
   * @defaultValue null
   */
  errorMsg?: string | null;
  /**
   * For fields belonging to this form that don't exist in the fields array, what their initial values should be
   */
  additionalDefaultValues?: { [key: string]: any };
  /**
   * Mapping of short names for field groups to display names
   */
  fieldGroupDisplayNames?: Map<string, string>;
};

/**
 * Returns the proper component for the field type coming from {@link GenericFormItem}.
 *
 * @param {GenericFormItemProps} props
 *
 * @remarks
 * For internal use only
 */
function GenericFormItemComp(props: GenericFormItemProps) {
  const commonProps = {
    fcName: props.fcName,
    type: props.type,
    name: props.name,
    value: props.value,
    required: props.required,
    error: props.error,
    errorMessage: props.errorMessage,
    disabled: props.formDisabled || props.disabled,
    tooltip: props.tooltip,
    tooltipZIndex: props.tooltipZIndex,
  };

  if (props.type === 'custom') {
    if (!props.component) {
      throw Error('Must provide "component" as a prop!');
    }

    return props.component;
  } else if (props.type === 'date') {
    return <DateField {...commonProps} formik={props.formik} />;
  } else if (props.type === 'dropdown' && props.options) {
    if (!props.options) {
      throw Error('Must provide "options" as a prop!');
    }

    if (!props.dropdownLabelFunctionPred) {
      throw Error('Must provide "dropdownLabelFunctionPred as a prop!');
    }

    if (!props.optionKey) {
      throw Error('Must provide "optionKey" as a prop!');
    }

    if (!props.optionValueKey) {
      throw Error('Must provide "optionValueKey" as a prop!');
    }

    return (
      <GenericDropdown
        {...commonProps}
        onValueChange={props.setValue}
        options={props.options}
        optionKey={props.optionKey}
        optionValueKey={props.optionValueKey}
        optionValueKeyPrefix={props.optionValueKeyPrefix}
        labelFunctionPred={props.dropdownLabelFunctionPred}
        multiple={props.multiple}
        onInputChange={props.onInputChange}
        clickableErrorMessage={props.clickableErrorMessage}
      />
    );
  } else if (props.type === 'text' || props.type === 'number') {
    return (
      <GenericTextField
        {...commonProps}
        onValueChange={props.handleChange}
        inputProps={props.inputProps}
        clickableErrorMessage={props.clickableErrorMessage}
      />
    );
  } else if (props.type === 'multiline') {
    return (
      <GenericTextField
        {...commonProps}
        onValueChange={props.handleChange}
        multiLine={true}
        clickableErrorMessage={props.clickableErrorMessage}
      />
    );
  } else {
    return <></>;
  }
}

type GenericFormHeaderProps = {
  title: string;
  variant?: 'h6' | 'body1';
  actionButtonConfig?: {
    title: string;
    icon: ReactElement;
    action: () => Promise<void>;
  };
};

function GenericFormHeader(props: GenericFormHeaderProps) {
  props = {
    ...props,
    variant: !props.variant ? 'h6' : props.variant,
  };

  const formTitleWidth = props.actionButtonConfig ? 8 : 12;
  const formTitle = (
    <Grid item xs={formTitleWidth}>
      <Typography
        variant={props.variant}
        sx={{ textAlign: 'left', color: 'primary.main' }}
      >
        {props.title}
      </Typography>
    </Grid>
  );

  if (!props.actionButtonConfig) {
    return <Grid container>{formTitle}</Grid>;
  } else {
    return (
      <Grid container>
        {formTitle}
        <Grid
          container
          item
          xs={4}
          direction="column"
          alignItems="flex-end"
          sx={{ mt: -3 }}
        >
          <ActionButton
            title={props.actionButtonConfig.title}
            startIcon={props.actionButtonConfig.icon}
            onButtonClick={props.actionButtonConfig.action}
          />
        </Grid>
      </Grid>
    );
  }
}

function GenericFormItemCompRenderGroup(props: {
  titleMap: Map<string, string>;
  title: string;
  fields: Array<GenericFormItem>;
  formik: FormikProps<any>;
  disabled: boolean | undefined;
  boxStyle: any;
}) {
  const collapsibleFields: Array<GenericFormItem> =
    new Array<GenericFormItem>();

  props.fields
    .filter((field) => field.collapsible !== undefined)
    .map((field) => {
      collapsibleFields.push(field);
    });

  return (
    <Box sx={props.boxStyle}>
      <GenericFormHeader
        title={props.titleMap.get(props.title)}
        variant="body1"
      />
      {props.fields
        .filter((field) => !collapsibleFields.includes(field))
        .map((field: GenericFormItem, key: number) => (
          <GenericFormItemCompRender
            key={key}
            field={field}
            formik={props.formik}
            disabled={props.disabled}
          />
        ))}
      {collapsibleFields.length !== 0 ? (
        <Accordion
          elevation={0}
          sx={{ mt: 1.5, '&:before': { display: 'none' } }}
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon color="primary" />}
            sx={{
              p: 0,
              color: props.boxStyle.borderColor,
              textDecoration: 'underline',
            }}
          >
            Additional Fields
          </AccordionSummary>
          <AccordionDetails sx={{ p: 0 }}>
            <Box sx={{ ...props.boxStyle, mt: 0, pt: 0 }}>
              {collapsibleFields.map((field: GenericFormItem, key: number) => (
                <GenericFormItemCompRender
                  key={key}
                  field={field}
                  formik={props.formik}
                  disabled={props.disabled}
                />
              ))}
            </Box>
          </AccordionDetails>
        </Accordion>
      ) : (
        <></>
      )}
    </Box>
  );
}

// TODO: refactor this and `GenericFormItemComp`
function GenericFormItemCompRender(props: {
  field: GenericFormItem;
  formik: FormikProps<any>;
  disabled: boolean | undefined;
}) {
  return (
    <GenericFormItemComp
      fcName={props.field.fcName}
      name={props.field.name}
      type={props.field.type}
      required={props.field.required}
      value={props.formik.values[props.field.fcName]}
      inputProps={props.field.inputProps}
      disabled={props.field.disabled}
      error={
        (props.formik.touched[props.field.fcName] &&
          Boolean(props.formik.errors[props.field.fcName])) ||
        props.field.error
      }
      errorMessage={
        (props.formik.touched[props.field.fcName] &&
          (props.formik.errors[props.field.fcName] as string)) ||
        props.field.errorMessage
      }
      handleChange={props.formik.handleChange}
      options={props.field.options}
      dropdownLabelFunctionPred={props.field.dropdownLabelFunctionPred}
      optionKey={props.field.optionKey}
      optionValueKey={props.field.optionValueKey}
      optionValueKeyPrefix={props.field.optionValueKeyPrefix}
      // @ts-ignore
      setValue={props.formik.setFieldValue}
      multiple={props.field.multiple}
      formDisabled={props.disabled}
      component={props.field.component}
      onInputChange={props.field.onInputChange}
      clickableErrorMessage={props.field.clickableErrorMessage}
      formik={props.formik}
      tooltip={props.field.tooltip}
      tooltipZIndex={props.field.tooltipZIndex}
    />
  );
}

/**
 * Generates a form with specified fields (see {@link GenericFormItem}).
 *
 * @param {GenericFormProps} props
 * @example
 * import { GenericForm } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import type { GenericFormItem } from '@oklahoma-biological-survey/obs-react-lib/nextjs/utils/types';
 *
 * export default function TestForm() {
 *   const dropdownOptions = [
 *     'Option 1',
 *     'Option 2'
 *   ];
 *
 *   const fields: Array<GenericFormItem> = [
 *     { fcName: 'field1', name: 'Field 1', type: 'dropdown', options: dropdownOptions, value: 'Option 1 },
 *     { fcName: 'field2', name: 'Field 2', type: 'text', value: '' }
 *   ];
 *
 *   const onSubmit = (values: any) => console.log(values);
 *
 *   return (
 *     <GenericForm
 *       title='Test Form'
 *       fields={fields}
 *       onSubmit={onSubmit}
 *       submitButtonTitle='Submit Test Form'
 *     />
 *   );
 * }
 */
export default function GenericForm(props: GenericFormProps) {
  props = {
    ...props,
    width: !props.width ? '75%' : props.width,
    borderColor: !props.borderColor ? '#000' : props.borderColor,
    errorMsg: !props.errorMsg ? null : props.errorMsg,
    disabled: !props.disabled ? false : props.disabled,
  };

  const defaultValues = props.fields.reduce((map, field) => {
    map[field.fcName] = field.value ? field.value : '';
    return map;
  }, props.additionalDefaultValues || {});

  const formik = useFormik({
    initialValues: defaultValues,
    validationSchema: props.validationSchema,
    onSubmit: (values) => handleSubmit(values),
  });

  // TODO: need to also check for valid status so that the callback is called upon first invalid validation
  useEffect(() => {
    if (!formik.isValid && props.handleValidationError) {
      props.handleValidationError();
    }
  }, [formik.submitCount]);

  const handleSubmit = async (values: any) => {
    await props.onSubmit(values);
    formik.setSubmitting(false);
  };

  const formGroups: Map<string, Array<GenericFormItem>> = new Map<
    string,
    Array<GenericFormItem>
  >();

  props.fields
    .filter((field) => field.formGroup !== undefined)
    .map((field) => {
      // field.formGroup will not be undefined, based on the filter predicate above
      if (formGroups.has(field.formGroup)) {
        formGroups.get(field.formGroup)?.push(field);
      } else {
        formGroups.set(field.formGroup, [field]);
      }
    });

  const boxStyle = {
    border: 2,
    borderColor: props.borderColor,
    borderRadius: '16px',
    p: 3,
    pt: 1.5,
    mt: 2,
    mx: 'auto',
    width: props.width,
  };

  return (
    <Box sx={boxStyle}>
      <GenericFormHeader
        title={props.title}
        actionButtonConfig={props.actionButtonConfig}
      />
      <FormikProvider value={formik}>
        <form onSubmit={formik.handleSubmit}>
          {formGroups.size === 0
            ? props.fields.map((field: GenericFormItem, index: number) => (
                <GenericFormItemCompRender
                  key={index}
                  field={field}
                  formik={formik}
                  disabled={props.disabled}
                />
              ))
            : Array.from(formGroups.entries()).map(
                ([formGroupName, fields], index: number) => (
                  <GenericFormItemCompRenderGroup
                    key={index}
                    titleMap={props.fieldGroupDisplayNames}
                    title={formGroupName}
                    fields={fields}
                    formik={formik}
                    disabled={props.disabled}
                    boxStyle={boxStyle}
                  />
                ),
              )}
          {props.submitBtnConfig && !props.submitRef ? (
            <ActionButton
              title={props.submitBtnConfig.title}
              startIcon={props.submitBtnConfig.icon}
              submitting={formik.isSubmitting}
              disabled={formik.isSubmitting || props.disabled}
            />
          ) : (
            <button
              ref={props.submitRef}
              type="submit"
              style={{ display: 'none' }}
            />
          )}
        </form>
      </FormikProvider>
      {props.errorMsg && (
        <Alert severity="error" sx={{ mt: 3 }}>
          {props.errorMsg}
        </Alert>
      )}
    </Box>
  );
}
