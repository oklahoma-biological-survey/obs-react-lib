import DownloadIcon from '@mui/icons-material/Download';
import { ColDef, ColGroupDef, SelectionChangedEvent } from 'ag-grid-community';
import 'ag-grid-community/styles/ag-grid.css';
import 'ag-grid-community/styles/ag-theme-quartz.css';
import { AgGridReact } from 'ag-grid-react';
import React, { useCallback, useMemo, useRef } from 'react';
import { ActionButton } from '../fields';

export type DataTableProps<T> = {
  /** The height of the grid */
  height: number;
  /** The data for this table */
  data: Array<T>;
  /** An object defining the columns. [See documentation](https://www.ag-grid.com/react-data-grid/column-definitions/) */
  columns: Array<ColDef<T> | ColGroupDef<T>>;
  /** The filename for exporting. If this is not set, exporting is disabled */
  exportName?: string;
  /**
   * Callback function for when a row is selected
   *
   * @defaultValue undefined
   */
  rowSelected?: {
    key: string;
    fn: (selectedId: string) => void;
  };
  /**
   * The strategy with which to use to autosize columns
   *
   * @defaultValue fitGridWidth
   */
  autoSizeStrategy?: 'fitCellContents' | 'fitGridWidth';
};

export default function DataTable<T>(props: DataTableProps<T>) {
  const gridRef = useRef<AgGridReact>(null);

  const defaultColDef = useMemo<ColDef>(() => {
    return {
      filter: 'agTextColumnFilter',
    };
  }, []);

  const exportCsv = useCallback(() => {
    if (!props.exportName) {
      throw Error('Must provide "exportName" prop');
    }

    gridRef.current?.api.exportDataAsCsv({
      fileName: `${props.exportName}-${new Date().toLocaleDateString('fr-CA')}`,
    });
  }, []);

  const handleSelectionChanged = (event: SelectionChangedEvent<T, any>) => {
    const selectedRows = event.api.getSelectedRows();

    if (!selectedRows || !selectedRows[0]) {
      return;
    }

    if (props.rowSelected) {
      const value = event.api.getSelectedRows()[0][props.rowSelected.key];

      props.rowSelected.fn(value);
    }
  };

  return (
    <div
      className={'ag-theme-quartz'}
      style={{ width: '100%', height: props.height, textAlign: 'left' }}
    >
      {props.exportName ? (
        <div style={{ margin: '10px 0' }}>
          <ActionButton
            title="Export"
            onButtonClick={exportCsv}
            startIcon={<DownloadIcon />}
          />
        </div>
      ) : (
        <></>
      )}
      <AgGridReact
        ref={gridRef}
        rowData={props.data}
        columnDefs={props.columns}
        autoSizeStrategy={{
          type: props.autoSizeStrategy || 'fitGridWidth',
        }}
        defaultColDef={defaultColDef}
        rowSelection={'single'}
        onSelectionChanged={handleSelectionChanged}
        enableCellTextSelection
      />
    </div>
  );
}
