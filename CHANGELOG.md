# Changelog

## Version 10.4.0 - May 28, 2024
  - Added
    - Ability to select text from cells in a data table

## Version 10.3.0 - May 19, 2024
  - Changed
    - Updated version of `next` to resolve vulnerabilites discovered by `npm audit`

## Version 10.2.0 - January 30, 2024
  - Added
    - Optional `onValueChange` prop to `DateField`
  - Changed
    - Type of `formik` prop on `DateField` to be optional

## Version 10.1.0 - January 23, 2024
  - Added
    - Ability to specify column groups
  - Fixed
    - Bug with row selection throwing error when it shouldn't

## Version 10.0.0 - January 22, 2024
  - Changed
    - Library that `DataTable` component uses from `mui-x-datagrid` to `ag-grid`

## Version 9.2.0 - January 18, 2024
  - Added
    - Ability to specify an action for `GenericSnackbar`s

## Version 9.1.1 - January 17, 2024
  - Fixed
    - Bug with `DateRange` field not updating parent value properly

## Version 9.1.0 - January 12, 2024
  - Added
    - Ability to collapse fields in a form group

## Version 9.0.1 - January 9, 2024
  - Fixed
    - Bug with `GenericDateField` not resetting when providing an empty value

## Version 9.0.0 - December 19, 2023
  - Added
    - Ability to hide/show fields in a `DataTable`
  - Removed
    - `GridColDef` export from `DataTable` component. It is now called `DataTableGridColDef`

## Version 8.6.0 - December 18, 2023
  - Added
    - Ability to group fields within a form

## Version 8.5.0 - November 28, 2023
  - Added
    - Ability to specify a default sort for data tables
  - Changed
    - Next.js version
  - Fixed
    - Audit vulnerabilities

## Version 8.4.0 - November 27, 2023
  - Added
    - Ability to specify a callbackUrl when logging in
  - Fixed
    - Ensuring value of text field is updated when the value prop changes

## Version 8.3.1 - November 20, 2023
  - Fixed
    - Value of `GenericTextField` not updating when its value prop changes

## Version 8.3.0 - November 9, 2023
  - Added
    - Ability to add a prefix to dropdown options

## Version 8.2.0 - November 6, 2023
  - Added
    - Ability to have clickable error messages on text and dropdown fields
    - Ability to set z-index for tooltips
    - Styling for snackbars

## Version 8.1.0 - October 30, 2023
  - Added
    - Tooltips to all field types

## Version 8.0.1 - October 29, 2023
  - Fixed
    - Type of argument for `DataTable`'s `cellSelected` function. Changes from `number` to `string`

## Version 8.0.0 - October 26, 2023
  - Added
    - Date field (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/157)
      - Added localization and prevented future dates from being selected (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/171)
    - Ability to create custom fields (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/158)
      - With validation (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/162)
    - Checkbox field (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/160)
      - Ability to use a checkbox outside of a form (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/169)
    - Ability to use fields outside of forms
      - Text fields (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/164)
      - Dropdowns (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/174)
    - Ability for dropdowns to have an asynchronous search (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/165)
    - Ability to disable specific options in a dropdown (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/175)
    - Ability to remove the clear button in a dropdown (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/176)
    - Date range field (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/178)
      - To be eventually replaced by MUI X date range picker (https://mui.com/x/react-date-pickers/date-range-picker/)
    - Ability to click on a specific column in a table (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/182)
    - Ability to specify a secondary action button for forms (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/184)
  - Fixed
    - Bug where number fields were changed upon scrolling (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/167)
    - Bug where dropdowns would not accept an empty string as a value (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/168)


## Version 7.0.0 - September 18, 2023
  - Added
    - Ability to not render a submit button for forms (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=bebcf4272fb620bb7f8c67a35dbbbfba97f8b56c)
      - Associated fix: https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=2b7678beac2b325e532819e23f38755054944027
    - Ability to pass a custom error message for a specific field from its form (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=63b7953afeddccc1c30b323700bd0447d077613d)
    - Common input adornments (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=e07b1e4587cf2be617a3858542ddfe3648b760c4)
    - Ability to pass objects to dropdowns instead of just strings (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=447eb33f42118483f5b46fc1e48130a26a907caa)
      - Associated fix: https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=e3e94204e73e15b3932d30662523a3e309d1b8a3
    - Ability to have numeric-only inputs (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=f9fb2b67bae4ce2610cad65ed5e690e769f06fe9)
    - Ability to handle validation errors in a form (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=055af5c39824326b219444f9b6114d7e7b2aa281)
    - Ability to disable entire forms and/or specific fields in a form (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=b378a39f2483babfcabb353c75cc5664b6924885)
  - Changed
    - Method in which input adornments are passed to fields (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=03eb67ccbf86cdbf477a78e308260065c7d9b5b0)
    - Using actual dividers instead of ASCII pipes for navbars (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=fa19014b31d39d2b11299fe9d7b843cb1be012c0)
  - Fixed
    - Ensured IDs on buttons are unique (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=44238ceb4fbf7a188ab05de795a204e982d63242)
    - Bug with chips not rendering properly in dropdowns with multiple selection enabled (https://gitlab.com/twalk-tech/react-lib/-/merge_requests/152/diffs?commit_id=bbb5fcb9164623d29a9b3a60d1fe1ac1256adc3c)
    - Miscellaneous issues with CI pipeline

## Version 6.2.0 - June 19, 2023
  - Adding ability to have a divider after a Navbar route

## Version 6.1.0 - May 22, 2023
  - Ensuring `yup` validation messages are properly displayed on fields

## Version 6.0.7 - May 13, 2023
  - Fixing bug with `GenericDropdown` selecting all fields by default

## Version 6.0.6 - May 12, 2023
  - Allowing any type for `onValueChange` in `GenericTextField`

## Version 6.0.5 - April 29, 2023
  - Fixing width of loading bar in `EsriMap` component
  - Fixing warning with `rowsPerPage` in `DataTable` component

## Version 6.0.4 - April 29, 2023
  - Ensuring `LoginForm` has 100% width in `NavbarLoginPopover`

## Version 6.0.3 - April 28, 2023
  - Changed fixed horizontal margins on `CustomTabs` to a width based on viewport with and dynamic horizontal margins
  - Removed bottom padding from Navbar
  - Ensuring Navbar options are styled properly and external links work
  - Centering the login form on the page
  - Fixed warning about objects in `NavbarDropdownOptions` not having a unique `key` prop
  - Fixed warning about `key` props being spread into JSX from MUI Autocomplete in `GenericDropdown`

## Version 6.0.2 - April 23, 2023
  - Added top margin back to `GenericForm`

## Version 6.0.1 - April 19, 2023
  - Changed `BasicUser` to accept a `firstName` and `lastName` rather than just `name`

## Version 6.0.0 - April 16, 2023
  - Changed how the user login icon is displayed
    - Accepts a BasicUser object on the login route, rather than calling getSession()
  - Changed StyledDivider to accept an MUI Theme as props instead of a color string
  - Removing misc TODOs and @ts-ignore statements
  - Fixing CI

## Version 5.0.0 - April 12, 2023
  - Added
    - Ability to specify default values for `GenericForm`s from the `fields` array, rather than a separate array
  - Removed
    - `defaultValues` prop from `GenericForm`

## Version 4.3.0 - April 6, 2023
  - Added
    - `ConfirmDialog` component

## Version 4.2.1 - March 31, 2023
  - Fixed
    - Ensuring the user avatar shows uppercase letters

## Version 4.2.0 - March 30, 2023
  - Added
    - Ability to specify a function for a route
  - Fixed
    - Avatar rendering for users who don't have a first and last name
    - Sign out functionality

## Version 4.1.3 - March 24, 2023
  - Fixed
    - Navbar links on mobile

## Version 4.1.2 - March 21, 2023
  - Fixed login form's function

## Version 4.1.1 - March 21, 2023
  - Fixed bug where form's submit button never stopped spinning
  - Fixed bug where a multiline input was displayed as a normal text field

## Version 4.1.0 - March 21, 2023
  - Adding ability to specify a text field should allow for multiline input
  - Fixed README

## Version 4.0.0 - March 20, 2023
  - Allowing logo to be resized

## Version 3.0.2 - March 14, 2023
  - Fixing package name and namespace

## Version 3.0.1 - March 14, 2023
  - Fixing CI

## Version 3.0.0 - March 2, 2023
  - Updated all components for use in an application with user authentication and administration

## Version 2.3.2 - February 23, 2023
  - Added
    - User profile link when user is authenticated
  - Changed
    - Moved `stringAvatar` function to utils

## Version 2.3.1 - February 23, 2023
  - Added
    - Documentation for `LoginForm`
  - Fixed
    - `LoginForm` exports

## Version 2.3.0 - February 22, 2023
  - Added
    - Support for `next-auth`

## Version 2.2.2 - February 21, 2023
  - Added
    - Documentation for `GenericForm` component
    - Example usage for `GenericDropdown` component

## Version 2.2.1 - February 21, 2023
  - Fixed
    - Hydration error in `GenericDropdown` component (#6)

## Version 2.2.0 - February 20, 2023
  - Added
    - `GenericDropdown` component
    - `GenericForm` component
    - `GenericFormItem` type
  - Changed
    - Made `onButtonClick` prop in `ActionButton` optional
    - Made `GenericTextField` usable in a `formik` form

## Version 2.1.1 - February 20, 2023
  - Removed
    - Top padding from `CustomTabs` component

## Version 2.1.0 - February 16, 2023
  - Added
    - Ability to listen for key presses when focused in a `GenericTextField` component

## Version 2.0.5 - February 16, 2023
  - Fixed
    - Bug with route paths being refreshed before an update has been processed

## Version 2.0.4 - February 13, 2023
  - Reverting v2.0.3

## Version 2.0.3 - February 13, 2023
  - Fixed
    - Bug with route paths being refreshed before an update has been processed

## Version 2.0.2 - February 12, 2023
  - Fixed
    - Bug with active routes on navbar not updating properly
  - Documented
    - Still existing bug with route paths being refreshed before an update has been processed

## Version 2.0.1 - February 7, 2023
  - Updated
    - README documentation to instruct users on how to install and use this package

## Version 2.0.0 - February 7, 2023
  - Added
    - Documentation to be deployed to pages site
  - Changed
    - Filenames to follow React conventions

## Version 1.2.0 - February 5, 2023
  - Added
    - `DataTable` component
    - `EsriMap` component
  - Fixed
    - Bug with `ExtendedDate` returning `NaN` if the date string passed in is an invalid date

## Version 1.1.0 - January 25, 2023
  - Added
    - `CustomTab` type
    - `CustomTabs` component
    - `ExtendedDate` class

## Version 1.0.1 - January 14, 2023
  - utils/types/route
    - Added
      - `activePathIndex` attribute to keep track of which path should be active for a given route
  - components/nav/navbar
    - Fixed
      - Bug when multiple routes had more than one path, when they should be linked together
      - Bug with paths not updating correctly when navigating back in the browser's history

## Version 1.0.0 - January 13, 2023
  - Added
    - Support for multiple paths (static or dynamic) to be defined for a single route
      - Changed `path` attribute (with type `string`) to `paths` (with type `Array<string>`) in `Route`
  - Removed
    - `actualPath` as an attribute from `Route`
    - `router` parameter from `NavbarProps`

## Version 0.6.0 - January 12, 2023
  - Updating all components and tests to support NextJS 13+

## Version 0.5.1 - January 1, 2023
  - Added ability to specify a value for `GenericTextField`s

## Version 0.5.0 - January 1, 2023
  - Added `GenericSnackbar` component

## Version 0.4.0 - December 27, 2022
  - Added ability to define a callback function for `onValueChange` in `GenericTextField`

## Version 0.3.3 - December 23, 2022
  - Finally fixing types

## Version 0.3.2 - December 22, 2022
  - Trying to fix types

## Version 0.3.1 - December 20, 2022
  - Adding typings

## Version 0.3.0 - December 19, 2022
  - Adding tests for entire library

## Version 0.2.1 - December 14, 2022
  - Patching CWE-94

## Version 0.2.0 - December 13, 2022
  - Adding unit testing
    - Still a WIP, only have tests for a few components

## Version 0.1.11 - December 6, 2022
  - Changing package name

## Version 0.1.10 - December 6, 2022
  - Fixing build to not have `dist/` in import path

## Version 0.1.9 - December 5, 2022
  - Trying to fix types

## Version 0.1.8 - December 5, 2022
  - Removing exports from types
  - Only publishing `dist/` foler
  - Using hosted runner to publish

## Version 0.1.7 - December 5, 2022
  - Changing order of hooks in Navbar

## Version 0.1.6 - December 5, 2022
  - Adding `useEffect` hook to ensure rehydration does not throw an error

## Version 0.1.5 - December 5, 2022
  - Adding babel plugin for emotion

## Version 0.1.4 - December 5, 2022
  - Fixing hydration issues with Navbar
  - Converting interfaces to types and exporting them

## Version 0.1.3 - December 5, 2022
  - Need to pass `NextRouter` as prop to navbar

## Version 0.1.2 - December 5, 2022
  - Using babel instead of rollup to build

## Version 0.1.1 - November 30, 2022
  - Library isn't working, trying some things to fix it

## Version 0.1.0 - November 28, 2022
  - Initial module definition
